<?php
/**
 * @file
 * uw_ct_program_admissions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ct_program_admissions_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_uw_ft_adm_image'.
  $field_bases['field_uw_ft_adm_image'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uw_ft_adm_image',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'entity_translation_sync' => array(
        0 => 'fid',
      ),
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_uw_ft_adm_lowbody'.
  $field_bases['field_uw_ft_adm_lowbody'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uw_ft_adm_lowbody',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'uw_ct_program_admissions'.
  $field_bases['uw_ct_program_admissions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'uw_ct_program_admissions',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'uw_ft_program_admissions',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'uw_ft_program_admissions_type',
  );

  return $field_bases;
}

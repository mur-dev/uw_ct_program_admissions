<?php
/**
 * @file
 * uw_ct_program_admissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_program_admissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_program_admissions content'.
  $permissions['create uw_ct_program_admissions content'] = array(
    'name' => 'create uw_ct_program_admissions content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_program_admissions content'.
  $permissions['delete any uw_ct_program_admissions content'] = array(
    'name' => 'delete any uw_ct_program_admissions content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_program_admissions content'.
  $permissions['delete own uw_ct_program_admissions content'] = array(
    'name' => 'delete own uw_ct_program_admissions content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_program_admissions content'.
  $permissions['edit any uw_ct_program_admissions content'] = array(
    'name' => 'edit any uw_ct_program_admissions content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_program_admissions content'.
  $permissions['edit own uw_ct_program_admissions content'] = array(
    'name' => 'edit own uw_ct_program_admissions content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'search uw_ct_program_admissions content'.
  $permissions['search uw_ct_program_admissions content'] = array(
    'name' => 'search uw_ct_program_admissions content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}

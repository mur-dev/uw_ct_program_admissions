<?php
/**
 * @file
 * uw_ct_program_admissions.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_program_admissions_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: node
  $overrides["node.uw_ct_program_admissions.has_title"] = 1;
  $overrides["node.uw_ct_program_admissions.help"] = '';
  $overrides["node.uw_ct_program_admissions.name"] = 'Admission requirements lookup';

 return $overrides;
}

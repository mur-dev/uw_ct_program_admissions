<?php
/**
 * @file
 * uw_ct_program_admissions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_program_admissions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info_alter().
 */
function uw_ct_program_admissions_node_info_alter(&$data) {
  if (isset($data['uw_ct_program_admissions'])) {
    $data['uw_ct_program_admissions']['has_title'] = 1; /* WAS: TRUE */
    $data['uw_ct_program_admissions']['help'] = ''; /* WAS: '' */
    $data['uw_ct_program_admissions']['name'] = 'Admission requirements lookup'; /* WAS: 'Program Admissions Type' */
  }
}
